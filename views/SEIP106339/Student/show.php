<?php
session_start();
include_once ("../../../" . "vendor/autoload.php");

use \App\BITM\SEIP106339\Student;
use \App\BITM\SEIP106339\Message;

$students = new Student();
$student = $students->show($_GET['id']);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <title>Student Registration</title>
        <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <!-- Bootstrap -->
                <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
                    </head>

                    <body>

                        <div class="container">
                            <table class="table table-bordered" class="table table-responsive" style="width:100%"height="20%">
                                <br>  <thead>
                                        <tr>
                                            <td><b>Sl.</b></td>
                                            <td><b>Name</b></td>		
                                            <td><b>Father name</b></td>
                                            <td><b>Mother name</b></td>
                                            <td><b>Birthday</b></td>
                                            <td><b>Gender</b></td>
                                            <td><b>Email</b></td>
                                            <td><b>Mobile no.</b></td>
                                            <td><b>Address</b></td>
                                            <td><b>Division</b></td>
                                            <td><b>Course</b></td>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr>
                                            <td><?php echo $student->id; ?></td>
                                            <td><?php echo $student->title; ?></td>		
                                            <td><?php echo $student->ftitle; ?></td>
                                            <td><?php echo $student->mtitle; ?></td>
                                            <td><?php echo $student->birth; ?></td>
                                            <td><?php echo $student->gender; ?></td>
                                            <td><?php echo $student->email; ?></td>
                                            <td><?php echo $student->mobile; ?></td>
                                            <td><?php echo $student->address; ?></td>
                                            <td><?php echo $student->division; ?></td>
                                            <td><?php echo $student->course; ?></td>
                                        </tr>

                                    </tbody>
                                    <tfoot><div><center><button type="button" class="btn btn-info"><a href="index.php">Go to list</a></button></center></div></tfoot><br>
                                        </table>
                                        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                                        <!-- Include all compiled plugins (below), or include individual files as needed -->
                                        <script src="../../../resource/bootstrap/css/bootstrap.min.css"></script>
                                        </div>
                                        </body>
                                        </html>
